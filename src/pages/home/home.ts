import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Post} from "../../interfaces/post";

const description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts:Post[] = [
    {
      avatar:"assets/imgs/avatar1.png",
      nick:"Luis Vilcarromero",
      timeago:"20 min",
      description,
      img:"assets/imgs/ionic.png"
    },{
      avatar:"assets/imgs/avatar1.png",
      nick:"Jose",
      timeago:"20 min",
      description,
    },{
      avatar:"assets/imgs/avatar1.png",
      nick:"Maria",
      timeago:"20 min",
      description,
      img:"assets/imgs/ionic.png"
    },
  ]

  constructor(public navCtrl: NavController) {

  }

}
