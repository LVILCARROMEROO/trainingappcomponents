export interface Post {
  avatar:string;
  nick:string;
  timeago:string;
  description:string;
  img?:string;
}
