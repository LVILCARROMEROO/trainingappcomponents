import { NgModule } from '@angular/core';
import { ExpandableComponent } from './expandable/expandable';
import {CommonModule} from "@angular/common";
import {IonicModule} from "ionic-angular";
import { PostComponent } from './post/post';
import { HeaderComponent } from './header/header';
@NgModule({
	declarations: [ExpandableComponent,
    PostComponent,
    HeaderComponent],
	imports: [CommonModule, IonicModule],
	exports: [ExpandableComponent,
    PostComponent,
    HeaderComponent]
})
export class ComponentsModule {}
