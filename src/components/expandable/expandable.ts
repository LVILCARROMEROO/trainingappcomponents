import {Component, Input} from '@angular/core';

@Component({
  selector: 'expandable',
  templateUrl: 'expandable.html'
})
export class ExpandableComponent {
  @Input() max: number = 100;
  @Input() text: string;
  show:boolean;

  constructor() {
    this.show = (this.text && this.text.length > this.max);
  }

  showToggle(){
    this.show = !this.show
  }

}
